# JS While

While 循环会在指定条件为真时循环执行代码块。

## 语法

```text
while (条件)
  {
  需要执行的代码
  }
```

