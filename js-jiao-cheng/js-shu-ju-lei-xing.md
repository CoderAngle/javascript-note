# JS 数据类型

## javascript数据类型

* 字符串

```text
var name="angle"
```

* 数字

```text
var number=1
```

* 布尔

```text
var flag = true
```

* 数组

```text
var myarray = new Array()
myarray[0] = "angle"
----
var arr = new Array("angle","miku")
```

* 对象

字典形式

```text
var person = {firstname:"Bill",lastname:"angle",id:223}
```

* NULL

```text
Undefined这个值表示变量不含有值
可以通过将变量的值设置为null来清空变量

name=null
```

* Undefined

## 声明变量类型

当声明新变量时，可以使用关键词"new"来声明其类型

```text
var myname = new String
```

